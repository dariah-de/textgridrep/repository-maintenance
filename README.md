# Repository Maintenance

This repository is to collect scripts and docs which are used to maintain the TextGrid repository.
Add a new subdirectory for each task prefixed by a date, and re-use old stuff if its useful.

## Changelogs

One idea is that every task which modifies data on disk adds an entry to a file named [tguri].changelog.
It should be formatted in [GNU style](https://www.gnu.org/prep/standards/html_node/Style-of-Change-Logs.html).

Look into [2021-07-05-fix-public-exif/fix-data.sh](./2021-07-05-fix-public-exif/fix-data.sh#L9) (line 8) and
[2021-07-05-fix-public-exif/changelogentry.txt](./2021-07-05-fix-public-exif/changelogentry.txt) for an example how
this could be done. Here is an example of one of these files created by that job.

        $ cat /data/public/productive/pairtree_root/te/xt/gr/id/+3/qz/jt/,2/textgrid+3qzjt,2.changelog 

        2021-07-06  Ubbo Veentjer  <veentjer@sub.uni-goettingen.de>
                
                Remove superfluous EXIF entries from metadata files, caused by https://projects.gwdg.de/projects/tg/work_packages/35593/activity
