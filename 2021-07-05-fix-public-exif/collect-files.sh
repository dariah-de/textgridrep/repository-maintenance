#!/bin/bash

OUTPUT_BASE_DIR=./es-dataingest-public/
MDOUT_DIR=${OUTPUT_BASE_DIR}metadata/

function dostuff {
    local GRIDFILE=$1
    
    # strip everything before the + which is path and 'textgrid+'
    local IN=$(echo $GRIDFILE | sed "s|^.*+||")

    # replace , by .
    local FNAME=${IN/,/.}
    
    # remove .meta suffix
    FNAME=${FNAME/.meta/}
    
    cp $1 $MDOUT_DIR$FNAME
}

if [ "$#" == "0" ]; then
    echo -e "\e[31mno listfile with filenames given\e[0m"
    echo "usage: $0 filenames.list"
    echo "filenames.list should be a file with textgrid-metadata files to collect"
    echo "directory es-dataingest-public and subdirs should be created before with create_dirs.sh"
    exit
fi

while read -r line; do 
  dostuff $line
done < $1
