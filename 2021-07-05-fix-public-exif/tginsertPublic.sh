#! /bin/bash

#------------------SCRIPT DESCRIPTION----------------------------------------------
#SCRIPT-NAME: tginsertPublic.sh-------------------------------------------------
#SCRIPT-FUNCTION: indexing data into the TG-ElasticSearch index for public data
#DATE: 29042013
#----------------------------------------------------------------------------------

ELASTIC_SEARCH_URL=http://localhost:9202/textgrid-public/metadata

recursivIngest ()
{
        counter=0

        for SRCFILE in $(find $1 -type f); do
        
                local ID=`basename $SRCFILE .json`

                local ENTRY_EXISTS=`curl -s -XGET $ELASTIC_SEARCH_URL/$ID | jq .found`
                
                if [[ $ENTRY_EXISTS == "true" ]]; then

                  echo -e "\nindexing $SRCFILE"

                  curl -XPUT $ELASTIC_SEARCH_URL/$ID -d @$SRCFILE

                  # cleanup wrong uris
		              #curl -XDELETE $ELASTIC_SEARCH_URL/$ID
		              
		            else
		              echo -e "\n\e[31mID does not yet exist in index: $ID \e[0m"
		            fi
		            
		            counter=$(( $counter +1 ))
        done

}

if [ $1"x" == "x" ]
then
        echo provide a directory which contains tgmetadata in json
        exit 1;
else
    echo -e "Starte den Indexierungsprozess der JSON Dateien \n\n"
    sleep 1
    recursivIngest $1
    echo Ingest finished $counter files
fi

