declare namespace rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#";

declare updating function local:myUpdate($elem as element())
{
  delete node $elem
};

for $e in //rdf:Description[starts-with(@rdf:about, 'file://')]
  return local:myUpdate($e)

