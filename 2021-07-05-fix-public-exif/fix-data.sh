#!/bin/bash


function dostuff {
    local METAFILE=$1
    local LOGENTRYFILE=$2

    # create a changelog entry
    CHANGELOG=${METAFILE/.meta/.changelog}
    echo -e "\n`date -I`  `cat $LOGENTRYFILE`" >> $CHANGELOG
    
    # and go!
    xqilla -u -i $METAFILE fix-rdf.xq
    
}

if [ "$#" -le "1" ]; then
    echo -e "\e[31mplease provide list of files to modify and the position of the changelog entry\e[0m"
    echo "usage: $0 filenames.list changelogentry.txt" 
    echo "filenames.list should be a file with textgrid-metadata files to collect"
    echo "changelogentry.txt should be a file with an entry in changelog format, without the leading date (this script puts the date)"
    exit
fi

while read -r line; do 
  dostuff $line $2
done < $1

