# Whats this task

Fix wrong RDF EXIF metadata as reported in ~~https://projects.gwdg.de/projects/tg/work_packages/35593/activity~~ https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/-/issues/198

# Required tools

The following tools are used by these scripts:

* [jq](https://stedolan.github.io/jq/)
* [xqilla](http://xqilla.sourceforge.net/HomePage)
* sed
* bash
* curl

## 1. Gather list of files with metadata problems

runs overnight:

        $ nohup grep -r "file:///home/storage/tomcat-crud/" /data/productive/ > wrong-rdf-grep.log &

## 2. Create list of files to modifiy from grep.log

note: in the case of this file it was ok to cut after char 70, your list may be different, be warnend ;-)

        $ for i in `cut -c -70 wrong-rdf-grep.log`; do echo /data/public/$i; done > files.txt

## 3. prepare elasticseach-index fix

        $ git clone https://@projects.gwdg.de/gitolite/dariah-de/tg/textgrid-repository/common.git
        $ cd common/esutils/
        $ mvn package -Pshaded
        $ cd ../..
        $ cp common/esutils/target/esutils-3.6.2-shaded.jar .

## 4. apply the metadata fix

check the changelogentry, repair it if necessary

        $ cat changelogentry.txt

run the command to fix metadata on the repository

note: this actually touches the metadata on disk, be sure to check double if it works, 
      you may first test with just one entry in files.txt

        $ sudo ./fix-data.sh files.txt changelogentry.txt
        
## 5. add changed metadata to elasticsearch index

        $ ./create_dirs.sh
        $ ./collect-files.sh files.txt
        $ java -jar esutils-3.6.2-shaded.jar tgrep es-dataingest-public/
        $ ./tginsertPublic.sh es-dataingest-public/json/

## 6. DONE, check the metadata in public repo
