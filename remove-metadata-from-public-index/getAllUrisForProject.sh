#!/bin/bash

TGSEARCH_URL=http://textgridlab.org/1.0/tgsearch-public/search
LIMIT=200

if [ $1"x" == "x" ]
then
  echo provide project id to list public uris for
  echo "e.g. $0 TGPR-dc542f81-d10b-d380-3a70-5474944e4466"
  exit 1;
else
  PROJECT_ID=$1
  curl "${TGSEARCH_URL}?q=*&filter=project.id:${PROJECT_ID}&limit=${LIMIT}" | xmllint --xpath "//*[local-name()='textgridUri']/text()" -
fi

