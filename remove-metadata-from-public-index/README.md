# Remove Metadata from Public ElasticSearch Index

This is a workaround to not show unwanted data in textgridrep portal until we have
created a delete workflow (see <https://projects.academiccloud.de/projects/dariah-de-intern/wiki/loschung-von-repository-inhalten-1>).

We keep a list of uris (maybe TExtGrid project IDs first) to be removed from public index here, and can rerun the process
after reindexing data from disk into elasticsearch (or use this list for final deletion later).

URIs to be removed from elasticsearch are listed in [2024-08-22-uris.txt](./2024-08-22-uris.txt), a list of projects to be removed from the overview are documented in [2024-08-22-projects.txt](./2024-08-22-projetcs.txt).

**if you are not on esx2, edit [getAllUrisForProject.sh](./getAllUrisForProject.sh) and modify the tgsearch url.**
**also if you have a single project with more then 200 objects check the limit in getAllUrisForProject.sh**

```bash
# gather uris
./getAllUrisForProject.sh TGPR-8b5209b5-211a-7de8-ebfd-53a2a8b744fe >> 2024-08-22-uris.txt
# delete
./es-public_remove_uris.sh 2024-08-22-uris.txt
```

to gather all uris for projects listed in `2024-08-22-projects_short.txt` do:

```bash
for uri in `cat 2024-08-22-projects_short.txt`; do ./getAllUrisForProject.sh $uri | sort >> 2024-08-22-uris.txt; done
```

