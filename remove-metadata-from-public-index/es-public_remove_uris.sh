#!/bin/bash

ELASTIC_SEARCH_URL=http://localhost:9202/textgrid-public/_doc

if [ $1"x" == "x" ]
then
        echo provide a filename which contains a list of uris to remove from public elasticsearch index
        exit 1;
else
    echo -e "deleting all from $1"
    while read -r line; do
      shorturi=${line#*textgrid:}
      echo "deleting $shorturi"
      curl -XDELETE ${ELASTIC_SEARCH_URL}/$shorturi 
    done < $1
fi

