# Published metadata missing from nonpublic search index?

This happens after complete reindex from repo, as the collectdata scripts do not know which published metadata
should also live in nonpublic index.

## related: reindex-urilist
You may also want to look at https://gitlab.gwdg.de/dariah-de/dariah-de-common/-/tree/develop/esutils/tools/textgrid/reindexUriList
if thats the tool you need.

## Create a list of uris to copy to nonpublic index

E.g. the collection textgrid:2skd4.0 and all its children are missing from the nonpublic index, but present in public index. 

Get all children of textgrid:2skd4.0 and write to uris.txt

        curl -s https://textgridlab.org/1.0/tgsearch-public/info/textgrid:2skd4.0/children | xmllint --xpath '//*[local-name()="textgridUri"]/text()' - > uris.txt

add collection uri itself

        echo textgrid:2skd4.0 >> uris.txt
        
# Copy entries from public to nonpublic elasticsearch index

**surely you look into the copy.sh script and first test on dev-system before running on production data, do you?**

copy all uris from uris.txt from public to nonpublic index

        ./copy.sh uris.txt

