#!/bin/bash

while read -r line; do
  shorturi=${line#*textgrid:}
  curl http://localhost:9202/textgrid-public/metadata/$shorturi | jq ._source | curl -XPUT -H "Content-Type: application/json" -d @- http://localhost:9202/textgrid-nonpublic/metadata/$shorturi
done < $1
