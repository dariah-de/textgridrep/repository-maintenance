# Add aggregation RDF to rdf4j index for all RDF between 2024-05-23 and 2024-06-05

see [TG-crud issue #342](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/-/issues/342), and [TG-crud release v12.0.0](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/-/releases/v12.0.0), was fixed in [TG-crud release v12.0.2](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/-/releases/v12.0.2).

Between both releases TG-crud did not add the aggregation rdf (ore:aggregates) and deletet them on update. Here comes the repair script:

## 1. Collect collection/edition/aggregation URIs where the RDF may be missing

```bash
for mime in text/tg.aggregation+xml text/tg.collection+tg.aggregation+xml text/tg.edition+tg.aggregation+xml; do java -jar /opt/dhrep/cli-tools.jar find-objects -m ${mime} -f 2024-05-22 -t 2024-06-06; done > uris.txt
```

## 2. Collect the OAI-ORE RDF in ./data

```bash
mkdir data
sudo ./collect-files.sh uris.txt
ls -alh data

# change owner
sudo chown -R sfunk ./data/

# remove metadata files
rm data/*.meta
```

if you find files with 0 byte, remove them

## 3. upload to rdf4j

```bash
for file in data/*; do curl -XPOST http://localhost:9091/openrdf-sesame/repositories/textgrid-nonpublic/statements --header 'Content-Type: application/rdf+xml' --data @$file; done
```
