#!/bin/bash

DHREP_SCRIPT_BASE=/opt/dhrep


OUTPUT_DIR=./data/

DATA_PATH=/data/nonpublic/productive/pairtree_root/te/xt/gr/id
SESAME_URL=http://localhost:9091/openrdf-sesame/repositories/textgrid-nonpublic


# source common functions and settings
source "${DHREP_SCRIPT_BASE}"/functions.d/textgrid-shared.sh
source "${DHREP_SCRIPT_BASE}"/functions.d/inspect.sh

function dostuff {
    local uri=$1
    local id=${uri#*:}
    local path=$(id2path $id)

    echo $path
    cp $path/* $OUTPUT_DIR

}

if [ "$#" == "0" ]; then
    echo -e "\e[31mno listfile with filenames given\e[0m"
    echo "usage: $0 filenames.list"
    echo "filenames.list should be a file with textgrid-uris to collect aggregation files for"
    exit
fi

while read -r line; do 
  dostuff $line
done < $1
