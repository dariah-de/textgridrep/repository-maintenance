# find-objects (aggregations*) updated between X and  Y

## dev.textgridlab.org

```bash
root@textgrid-esx1:/opt/dhrep# java -jar cli-tools.jar find-objects -m text/tg.aggregation+xml -f 2024-05-23T14:28:00 -t 2024-06-05T14:28
textgrid:484xz.0
textgrid:484z0.0
textgrid:4854n.0
textgrid:4854q.0
root@textgrid-esx1:/opt/dhrep# java -jar cli-tools.jar find-objects -m text/tg.collection+tg.aggregation+xml -f 2024-05-23T14:28:00 -t 2024-06-05T14:28
textgrid:4854r.0
textgrid:4854s.0
root@textgrid-esx1:/opt/dhrep# java -jar cli-tools.jar find-objects -m text/tg.edition+tg.aggregation+xml -f 2024-05-23T14:28:00 -t 2024-06-05T14:28
root@textgrid-esx1:/opt/dhrep#
```

## textgridlab.org

```bash
root@textgrid-esx2:/opt/dhrep# java -jar cli-tools.jar find-objects -m text/tg.aggregation+xml -f 2024-05-23T14:28:00 -t 2024-06-05T14:28
textgrid:3vq9c.0
textgrid:3w29s.0
textgrid:3w29t.0
textgrid:3w8qm.0
textgrid:3w8qn.0
textgrid:3wbh1.0
textgrid:3xrtf.0
textgrid:40qch.0
textgrid:40vr1.0
textgrid:410kj.0
textgrid:41s5c.0
textgrid:41s5d.0
textgrid:25539.0
textgrid:3rp6c.0
root@textgrid-esx2:/opt/dhrep# java -jar cli-tools.jar find-objects -m text/tg.collection+tg.aggregation+xml -f 2024-05-23T14:28:00 -t 2024-06-05T14:28
textgrid:46hjm.0
root@textgrid-esx2:/opt/dhrep# java -jar cli-tools.jar find-objects -m text/tg.edition+tg.aggregation+xml -f 2024-05-23T14:28:00 -t 2024-06-05T14:28
root@textgrid-esx2:/opt/dhrep#
```
